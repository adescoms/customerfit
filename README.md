#  Customer Fit 

Static web page powered by [hugo](https://gohugo.io)

To start the web locally: 
```
 hugo server -D
```

## Pipeline status

[![pipeline status](https://gitlab.com/adescoms/customerfit/badges/main/pipeline.svg)](https://gitlab.com/adescoms/customerfit/-/commits/main)
